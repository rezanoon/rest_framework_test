from django.conf.urls import url
from .api import views
from rest_framework.generics import ListCreateAPIView
from .api.serializers import PostSerializer, CommentSerializer, AnotherCommentSerializer
from .models import Post

app_name = 'blog'
urlpatterns = [
    url(r'^posts$', views.PostListView.as_view(), name='posts'),
    # url(r'^postss$', ListCreateAPIView.as_view(queryset=Post.objects.all(), serializer_class=PostSerializer), name='post_create'),
    url(r'^comments/(?P<pk>\d+)$', views.CommentDetailView.as_view(), name='comments'),
    # url(r'^post/(?P<id>\d+)$', views.get_post, name='post'),
    url(r'^comment/(?P<pk>\d+)$', views.CommentView.as_view(), name='comment'),
]
