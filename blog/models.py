from django.db import models


class Post(models.Model):
    name = models.CharField(blank=False, max_length=255)
    content = models.CharField(blank=False, max_length=255)



class Comment(models.Model):
    content = models.CharField(blank=False, max_length=255)
    post = models.ForeignKey(Post)

