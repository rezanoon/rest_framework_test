from rest_framework import generics
from ..models import Post,Comment
from rest_framework.views import APIView
from django.shortcuts import get_object_or_404
from .serializers import PostSerializer, CommentSerializer, AnotherCommentSerializer
from io import BytesIO
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt


class PostListView(generics.ListAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer


@csrf_exempt
class PostCreateView(generics.ListCreateAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer


class CommentView(generics.RetrieveAPIView):
    queryset = Comment.objects.all()
    serializer_class = AnotherCommentSerializer



class CommentDetailView(APIView):
    def get(self, request, pk, format=None):
        comment = get_object_or_404(Comment, pk=pk)
        comment = CommentSerializer(comment)
        return Response(comment.data)


# class PostList(generics.ListCreateAPIView):
#     queryset = User.objects.all()
#     serializer_class = PostSerializer
#     permission_classes = (IsAdminUser,)

#     def list(self, request):
#         # Note the use of `get_queryset()` instead of `self.queryset`
#         queryset = self.get_queryset()
#         serializer = PostSerializer(queryset, many=True)
#         return Response(serializer.data)



