from rest_framework import serializers
from ..models import Post,Comment

class PostSerializer(serializers.ModelSerializer):
    class Meta: 
        model = Post
        fields = ('id', 'name', 'content')


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ('id', 'content', 'post')


class AnotherCommentSerializer(serializers.ModelSerializer):
    post = PostSerializer(many=False, read_only=True)

    class Meta:
        model = Comment
        fields = ('id', 'content', 'post')
    
        
